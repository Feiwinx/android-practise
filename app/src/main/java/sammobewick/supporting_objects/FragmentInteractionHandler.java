package sammobewick.supporting_objects;

import java.util.ArrayList;

/**
 * Created by Sam on 06/06/2015.
 * This is a all-in-one interface that I created for the MainActivity to inherit.
 * It will contain methods that each Fragment may want to call upon from the activity.
 */
public interface FragmentInteractionHandler  {

    void    favouriteCurrent(Quote current);
    void    onListItemPressed(int position);
    void    editQuote(Quote quote);
    void    deleteQuote(Quote quote);

    ArrayList<Quote> getQuoteBook();
}
