package sammobewick.supporting_objects;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import sammobewick.practise_fragments.R;

/**
 * Created by Sam on 05/06/2015.
 * This class acts as an adapter for a list of quotes.
 * We have an inner class - ViewHolder - which contains the actual views we defined
 * in the XML file (in this project it's quote_row.xml).
 */
public class QuoteAdapter extends ArrayAdapter<Quote> {

    // Inner class, as mentioned, to hold our views.
    private class ViewHolder {
        TextView txt_quote;
        TextView txt_owner;
    }

    // We just use the constructor, but make sure you change textViewResourceId's type!
    public QuoteAdapter(Context context, int resource, ArrayList<Quote> textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    // We can have listeners here to handle presses.

    // This is where the main part of this class happens. Basically gets the view, uses the ViewHolder
    // to track each row's attributes. Then we just set the strings.
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v              = convertView;
        ViewHolder holder   = new ViewHolder();

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.quote_row, null);

            // Here we assign the holder attributes:
            holder.txt_owner = (TextView) v.findViewById(R.id.item_owner);
            holder.txt_quote = (TextView) v.findViewById(R.id.item_quote);

            // Allows us to reference it when v == null:
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        Quote q = getItem(position);
        holder.txt_quote.setText(q.getQuote());
        holder.txt_owner.setText(q.getOwner());

        return v;
    }
}
