package sammobewick.supporting_objects;

/**
 * Created by Sam on 06/06/2015.
 * Allows me to refer to different fragments under one type definition.
 * They should implement their own version of this update method!
 */
public interface FragmentNotifier {

    void Update();
}
