package sammobewick.supporting_objects;

/**
 * Created by Sam on 05/06/2015.
 * This class represents the object that is a quote.
 * Important parts are that it contains the string quote, and string owner.
 */
public class Quote {

    private String  quote;
    private String  owner;

    // Not sure if the best way of keeping track of this. But hey-ho, learning.
    private Boolean favourite;

    // Constructor for creation.
    public Quote(String quote, String owner) {
        this.quote = quote;
        this.owner = owner;
        this.favourite = false;
    }

    public String getQuote() {
        return quote;
    }

    public String getOwner() {
        return owner;
    }

    public Boolean getFavourite() {
        return favourite;
    }

    // The only set method, so it can be changed whenever.
    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

    @Override
    public String toString() {
        return owner + ":" + quote + ":" + String.valueOf(favourite);
    }
}
