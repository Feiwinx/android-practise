package sammobewick.practise_fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;

import java.util.ArrayList;

import sammobewick.supporting_objects.FragmentInteractionHandler;
import sammobewick.supporting_objects.FragmentNotifier;
import sammobewick.supporting_objects.Quote;
import sammobewick.supporting_objects.QuoteAdapter;

/**
 * A fragment representing a list of Items.
 */
public class ListFragment extends android.support.v4.app.Fragment
        implements  AbsListView.OnItemClickListener,
                    FragmentNotifier
{
    private FragmentInteractionHandler mListener;

    private int tab_no;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private ListAdapter mAdapter;

    ArrayList<Quote>    quoteBook;      // Used to store our list of quotes.
    int                 quoteCount;     // Keeps track of which quote we are viewing.

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Here we get the parent, list of quotes, and initialise the count:
        quoteCount  = 0;

        if (getArguments() != null) {
            tab_no = getArguments().getInt("TAB");
        }

        this.Update();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);

        mListView.setEmptyView(view.findViewById(android.R.id.empty));

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (FragmentInteractionHandler) activity;
            this.Update();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.Update();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            mListener.onListItemPressed(position);
        }
    }

    @Override
    public void Update() {
        if (tab_no == 2) {
            ArrayList<Quote> tempQuoteBook = mListener.getQuoteBook();
            quoteBook = new ArrayList<>();

            for (Quote q : tempQuoteBook) {
                if (q.getFavourite()) {
                    quoteBook.add(q);
                }
            }
        } else {
            quoteBook = mListener.getQuoteBook();

        }
        mAdapter = new QuoteAdapter(getActivity(),
                R.layout.quote_row, quoteBook);
    }
}