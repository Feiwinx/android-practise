package sammobewick.practise_fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import sammobewick.supporting_objects.FragmentInteractionHandler;
import sammobewick.supporting_objects.FragmentNotifier;
import sammobewick.supporting_objects.Quote;

public class MainActivity extends AppCompatActivity
        implements FragmentInteractionHandler
{
    private static String FILENAME = "quotebook.dat";
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    ArrayList<Quote> quoteBook;     // stores our quotes.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);     // ...
        setContentView(R.layout.activity_main); // Set our content view.

        // Create the adapter that will return a fragment for each section.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        LoadData();
    }

    private void SaveData() {
        try {
            File data = new File(getFilesDir() + "/" + FILENAME);

            //if (!data.exists()) { data.createNewFile(); }

            BufferedWriter  br      = new BufferedWriter(
                    new FileWriter(data.getAbsoluteFile()));

            for (Quote q : quoteBook) {
                br.write(q.toString());
                br.newLine();
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void LoadData() {
        quoteBook   = new ArrayList<>();
        try {
            String line;
            BufferedReader in = new BufferedReader(
                    new FileReader(getFilesDir() + "/" + FILENAME));

            while ((line = in.readLine()) != null) {
                Quote q = readQuoteFromLine(line);
                if (q != null) {
                    quoteBook.add(q);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Quote readQuoteFromLine(String l) {
        if (l.contains(":")) {
            Quote rQuote;
            String[] split = l.split(":");

            rQuote = new Quote(split[1], split[0]);

            Boolean fav = Boolean.valueOf(split[2]);
            rQuote.setFavourite(fav);

            return rQuote;
        } else { return null; }
    }

    @Override
    public void onPause() {
        super.onPause();
        SaveData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // Persistent actions across the activity (a fragment can add it's own).
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_new) {
            quoteDialog(null);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // quoteDialog is used to add a quote to our array.
    private void quoteDialog(final Quote editable) {
        final Boolean editing;
        int DIALOG_LAYOUT = R.layout.action_add_quote;

        editing = null != editable;

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(DIALOG_LAYOUT);
        dialog.setTitle("Enter New Quote");
        dialog.show();

        final EditText  edit_quote  = (EditText)    dialog.findViewById(R.id.edit_quote);
        final EditText  edit_owner  = (EditText)    dialog.findViewById(R.id.edit_owner);
        final Button    btn_add     = (Button)      dialog.findViewById(R.id.btn_add);
        final Button    btn_dis     = (Button)      dialog.findViewById(R.id.btn_discard);

        if (editing) {
            edit_owner.setText(editable.getOwner());
            edit_quote.setText(editable.getQuote());
            btn_add.setText("Confirm");
            btn_dis.setText("Discard");
        }

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txt_q    = edit_quote.getText().toString();
                String txt_o    = edit_owner.getText().toString();

                if (txt_o.length() > 0 & txt_q.length() > 0) {
                    Quote new_q = new Quote(txt_q, txt_o);
                    if (editing) {
                        for (Quote q : quoteBook) {
                            if (editable.equals(q)) {
                                int i = quoteBook.indexOf(q);
                                quoteBook.set(i, new_q);
                            }
                        }
                    } else { quoteBook.add(new_q); }
                    dialog.dismiss();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter a quote and a owner!",
                            Toast.LENGTH_SHORT).show();
                }
                notifyFragments();
            }
        });

        btn_dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        System.out.println("QUOTEDIALOG-FINISHED");
    }

    // This method was sourced from Stack Overflow:
    // http://stackoverflow.com/a/14039416 //
    private String getFragmentTag(int fragmentPosition)
    {
        return "android:switcher:" + mViewPager.getId() + ":" + fragmentPosition;
    }

    private void notifyFragments() {
        System.out.println("NOTIFYFRAGMENTS-STARTED");
        int fragmentCount = getResources().getStringArray(R.array.tab_titles).length;

        for (int i = 0; i < fragmentCount; i++) {
            String tag = getFragmentTag(i);
            FragmentNotifier f = (FragmentNotifier)
                    getSupportFragmentManager().findFragmentByTag(tag);
            if (f != null) { f.Update(); }
        }
    }

    @Override
    public void favouriteCurrent(Quote current) {
        if (quoteBook.contains(current)) {
            for (Quote q : quoteBook) {
                if (current.equals(q)) {
                    if (q.getFavourite()) {
                        q.setFavourite(false);
                    }
                    else { q.setFavourite(true); }
                    break;
                }
            }
        } else {
            Toast.makeText(this,
                    "ERROR - failed to find this quote in the book.",
                    Toast.LENGTH_SHORT).show();
        }
        notifyFragments();
    }

    @Override
    public void onListItemPressed(int position) {
        mViewPager.setCurrentItem(0);

        String tag = getFragmentTag(0);
        PreviewFragment frag = (PreviewFragment) getSupportFragmentManager().findFragmentByTag(tag);
        frag.setQuoteNumb(position);
        notifyFragments();
    }

    @Override
    public void editQuote(Quote quote) {
        if (quoteBook.contains(quote)) {
            quoteDialog(quote);
        } else {
            Toast.makeText(this,
                    "ERROR - failed to find quote in book. Try creating a new one.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void deleteQuote(Quote quote) {
        if (quoteBook.contains(quote)) {
            quoteBook.remove(quote);
        } else {
            Toast.makeText(this,
                    "ERROR - failed to find quote in book.",
                    Toast.LENGTH_SHORT).show();
        }
        notifyFragments();
    }

    @Override
    public ArrayList<Quote> getQuoteBook() {
        return quoteBook;
    }

    /////*  SECTIONS PAGER ADAPTER  */////

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final String[] titles;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);

            // Get the titles array:
            titles = getResources().getStringArray(R.array.tab_titles);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag;

            // Create fragment, depending on position:
            if  (position == 0) {frag = new PreviewFragment();}
            else                {frag = new ListFragment();}

            Bundle args = new Bundle();
            args.putInt("TAB", position);
            frag.setArguments(args);

            return frag;
        }

        @Override
        public int getCount() { return titles.length; }

        @Override
        public CharSequence getPageTitle(int position) { return titles[position]; }
    }
}
