package sammobewick.practise_fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import sammobewick.supporting_objects.FragmentInteractionHandler;
import sammobewick.supporting_objects.FragmentNotifier;
import sammobewick.supporting_objects.Quote;

public class PreviewFragment extends android.support.v4.app.Fragment
    implements View.OnClickListener, FragmentNotifier
{
    private FragmentInteractionHandler mListener;

    /////*  VIEWS  */////
    private View        self;
    private TextView    text_quote;
    private TextView    text_owner;

    /////*  QUOTE-BOOK  */////
    private ArrayList<Quote>    quoteBook;
    private int                 quoteNumb;

    // Set Quote Number:
    public void setQuoteNumb(int quoteNumb) {
        this.quoteNumb = quoteNumb;
        displayQuote();
    }

    // Constructor:
    public PreviewFragment() {/* Not Empty */}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true); // This ensures we call the onCreateOptionsMenu function.
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {   // Inflate the layout for this fragment
        View v  = inflater.inflate(R.layout.fragment_preview, container, false);
        self    = v;

        // Get the TextViews we are using to display.
        text_owner = (TextView) v.findViewById(R.id.preview_owner);
        text_quote = (TextView) v.findViewById(R.id.preview_quote);

        // Create a listener on the preview tab:
        self.setOnClickListener(this);

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (FragmentInteractionHandler) activity;
            quoteBook = mListener.getQuoteBook();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onPause() {
        super.onResume();
        quoteBook = mListener.getQuoteBook();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_preview, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_fav) {
            if (quoteBook.size() > 0) {
                mListener.favouriteCurrent(quoteBook.get(quoteNumb));
                displayQuote();
            }
        } else if (id == R.id.action_edit) {
            if (quoteBook.size() > 0) {
                mListener.editQuote(quoteBook.get(quoteNumb));
            }
        } else if (id == R.id.action_delete) {

            mListener.deleteQuote(quoteBook.get(quoteNumb));
            quoteNumb = 0;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.preview_tab) {
            if (quoteNumb >= (quoteBook.size() - 1)) { quoteNumb = 0; }
            else { quoteNumb++; }

            displayQuote();
        }
    }

    private void displayQuote() {
        System.out.println("DISPLAYQUOTE-CALLED");
        if (quoteBook.size() > 0) {
            Quote q = quoteBook.get(quoteNumb);

            text_quote.setText(q.getQuote());
            text_owner.setText(q.getOwner());

            // Change colour to show a favourite quote [temporary function].
            if (q.getFavourite()) {
                self.setBackgroundColor(getResources().getColor(R.color.darkorange));
            } else {
                self.setBackgroundColor(getResources().getColor(R.color.darkpurple));
            }
        } else {
            text_quote.setText(R.string.lbl_empty_array);
            text_owner.setText("");
            self.setBackgroundColor(getResources().getColor(R.color.primary_dark_material_dark));
            quoteNumb = 0;
        }
    }

    @Override
    public void Update() {
        quoteBook = mListener.getQuoteBook();
        this.displayQuote();
    }
}
